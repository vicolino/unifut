Gurizada montamos um regulamento do nosso futebol, afim de esclarecer dúvidas e trazer maior organização para as nossas peladas. A partir do mês de maio adotaremos esse regulamento. 

 *Objetivo/Proposito do UniFut*
 
- Promover integração entre as pessoas que participam do grupo do futebol.

 *Do regulamento*
 
- Cabe a cada integrante da pe*ada respeitar e cumprir as regras descritas nesse regulamento.

 *Presidência*
 
- Diego Barão e Alessandro Silva (Afastado temporariamente)

 *Dia/Horário* 
 
- Todas as quartas (18:30 – 19:30)

 *Regras do jogo* 
 
- Deve ter no mínimo 14 e no máximo 16 jogadores por jogo.
- Substituições devem ocorrer a cada 7 minutos (se o jogador que estiver fora desejar entrar no jogo nesse intervalo de tempo), sendo um jogador de linha e também goleiro (caso não haja goleiro fixo). Cada time é responsável por controlar o tempo e realizar as trocas.

 *Mensalistas* 
 
- Devem pagar (15 reais * número total de jogos do mês) até o dia da primeira pelada do mês. Caso não pague até este dia, será considerado como diarista no restante do mês.
- Ganhará desconto ou não pagará pelos churrascos promovidos no futebol (depende do caixa do time – atualmente temos pouco mais de 30 reais no caixa) 
- Tem vaga garantida no futebol até a terça-feira (12 horas) que antecede o jogo.

 *Diaristas* 
 
- Devem pagar 18 reais por jogo.
- Não possui vaga garantida no futebol. Caso a lista do futebol esteja fechada e um mensalista confirme a sua presença no jogo até a terça-feira (12:00), automaticamente o último diarista a confirmar presença no futebol será excluído do evento. 
- Montaremos uma fila de espera de diaristas que queiram virar mensalistas (teremos no máximo 16 mensalistas). 

 *Escolha dos times* 
 
- Será realizada por no mínimo dois peladeiros no dia do jogo, considerando a qualidade/posição de cada participante da pelada (não realizaremos mais a prática do par ou ímpar)

 *Punições* 
 
- Se o peladeiro desistir de jogar a partida após as 12:00 da terça-feira que antecede o jogo ou confirme que irá  jogar e se ausente da partida, deverá pagar a multa de 20 reais. Enquanto não pagar o seu débito estará excluído do grupo e não poderá participar de novas peladas. 
- Se o peladeiro desistir de jogar a partida após as 12:00 da terça-feira que antecede o jogo, porém sua vaga for preenchida por outra pessoa a multa será cancelada.
- O peladeiro que se envolver em briga (agressão física) será automaticamente excluído do grupo.